@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>
            Transcription Report
        </h1>

        @include('transcription::report.transcription.table.window')

        @include('transcription::report.transcription.table.transcribed-videos')

    </div>
@endsection
