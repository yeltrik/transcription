<h2>
    Transcribed Videos
</h2>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Reporting Window</th>
        <th scope="col"># Videos</th>
        <th scope="col">Transcription Time</th>
    </tr>
    </thead>
    <tbody>
    @foreach($transcribedVideosTableArray as $key => $subArray)
        <tr>
            <th scope="row">{{$key}}</th>
            <td>{{$subArray['video_count']}}</td>
            <td>{{$subArray['transcription_time']}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
