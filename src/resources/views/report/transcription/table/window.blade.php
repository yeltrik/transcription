<h2>
    Reporting Windows
</h2>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Title</th>
        <th scope="col">Open</th>
        <th scope="col">Close</th>
        <th scope="col">Duration (Days)</th>
    </tr>
    </thead>
    <tbody>
    @foreach($reportWindows as $key => $reportWindow)
    <tr>
        <th scope="row">{{$key}}</th>
        <td>{{$reportWindow['start']->format('Y-m-d')}}</td>
        <td>{{$reportWindow['end']->format('Y-m-d')}}</td>
        <td>{{date_diff($reportWindow['start'],$reportWindow['end'])->format('%a days')}}</td>
    </tr>
        @endforeach
    </tbody>
</table>
