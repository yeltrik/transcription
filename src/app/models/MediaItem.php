<?php

namespace Yeltrik\Transcription\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaItem extends Model
{
    use HasFactory;

    protected $connection = 'transcription';
    public $table = 'media_item';

}
