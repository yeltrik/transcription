<?php

namespace Yeltrik\Transcription\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TranscribeStat extends Model
{
    use HasFactory;

    protected $connection = 'transcription';
    public $table = 'transcribe_stats';

}
