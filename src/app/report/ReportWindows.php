<?php


namespace Yeltrik\Transcription\app\report;


class ReportWindows
{

    /**
     * @return \DateTime[][]
     */
    public static function getWindows()
    {
        return [
            '2017-2018' => [
                'start' => new \DateTime('2017-08-21'),
                'end' => new \DateTime('2018-08-27')
            ],
            '2018-2019' => [
                'start' => new \DateTime('2018-08-27'),
                'end' => new \DateTime('2019-08-26')
            ],
            '2019-2020' => [
                'start' => new \DateTime('2019-08-26'),
                'end' => new \DateTime('2020-08-24')
            ],
            '2020-2021' => [
                'start' => new \DateTime('2020-08-24'),
                'end' => new \DateTime('2021-08-06')
            ],
            'Winter 2020' => [
                'start' => new \DateTime('2020-01-06'),
                'end' => new \DateTime('2020-01-27')
            ],
            'Spring 2020' => [
                'start' => new \DateTime('2020-01-27'),
                'end' => new \DateTime('2020-05-18')
            ],
            'Summer 2020' => [
                'start' => new \DateTime('2020-05-18'),
                'end' => new \DateTime('2020-08-24')
            ],
            'Fall 2020' => [
                'start' => new \DateTime('2020-08-24'),
                'end' => new \DateTime('2020-12-31')
            ]
        ];
    }

    /**
     * @return array
     */
    public static function durationsByWindow()
    {
        $durations = [];
        foreach (ReportWindows::getWindows() as $key => $window) {
            $start = $window['start'];
            $end = $window['end'];

            $mediaItemDuration = new MediaItemDuration();
            $mediaItemDuration->setWindowOfActivity($start, $end);
            $durations[$key] = $mediaItemDuration->totalDuration();
        }

        return $durations;
    }

}
