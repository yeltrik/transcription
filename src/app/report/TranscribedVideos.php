<?php


namespace Yeltrik\Transcription\app\report;


use Illuminate\Database\Eloquent\Builder;
use Yeltrik\Transcription\app\models\MediaItem;
use Yeltrik\Transcription\app\models\TranscribeStat;

class TranscribedVideos
{

    private ?ReportWindows $reportWindows = NULL;

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @return bool
     */
    private function _bothActivityWindowsSet(\DateTime $start, \DateTime $end)
    {
        return ($start !== NUll && $end !== NULL);
    }

    /**
     * @param Builder $query
     * @param \DateTime $start
     * @param \DateTime $end
     */
    private function _buildForActivity(Builder &$query, \DateTime $start, \DateTime $end)
    {
        if ($this->_bothActivityWindowsSet($start, $end)) {
            $this->_buildForActivityWindows($query, $start, $end);
        }
    }

    /**
     * @param Builder $query
     * @param \DateTime $start
     * @param \DateTime $end
     * @return Builder
     */
    private function _buildForActivityWindows(Builder &$query, \DateTime $start, \DateTime $end)
    {
        return $query
            ->whereIn('id', TranscribeStat::query()
                ->whereDate('timeSaved', '>=', $start)
                ->whereDate('timeSaved', '<', $end)
                ->pluck('mediaItemID')
                ->toArray()
            );
    }

    /**
     * @param ReportWindows $reportWindows
     */
    public function forWindows(ReportWindows $reportWindows)
    {
        $this->reportWindows = $reportWindows;
    }

    /**
     * @return array
     */
    public function getAsTableArray(): array
    {
        $array = [];
        foreach ( $this->reportWindows::getWindows() as $key => $subArray)
        {
            $start = $subArray['start'];
            $end = $subArray['end'];
            $array[$key] = [
                'video_count' => $this->videosForWindow($start, $end)->count(),
                'transcription_time' => round($this->transcriptionTimeForWindow($start, $end) / 60,0) . " minutes"
            ];

        }
        return $array;
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function mediaItemQueryForWindow(\DateTime $start, \DateTime $end)
    {
        $query = MediaItem::query();

        $this->_buildForActivity($query, $start, $end);

        return $query;
    }

    private function transcribeStatsQueryForWindow(\DateTime $start, \DateTime $end)
    {
        return TranscribeStat::query()
            ->whereDate('timeSaved', '>=', $start)
            ->whereDate('timeSaved', '<', $end);
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @return int
     */
    private function transcriptionTimeForWindow(\DateTime $start, \DateTime $end)
    {
        return $this->transcribeStatsQueryForWindow($start, $end)
            ->sum('totalTime');
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function videosForWindow(\DateTime $start, \DateTime $end)
    {
        return $this->mediaItemQueryForWindow($start, $end);
    }

}
