<?php

namespace Yeltrik\Transcription\app\report;


use Illuminate\Database\Eloquent\Builder;
use Yeltrik\Transcription\app\models\MediaItem;
use Yeltrik\Transcription\app\models\TranscribeStat;

class MediaItemDuration
{

    private ?\DateTime $activityWindowStart = NULL;
    private ?\DateTime $activityWindowEnd = NULL;

    /**
     * @return bool
     */
    private function _bothActivityWindowsSet()
    {
        return ($this->activityWindowStart !== NUll && $this->activityWindowEnd !== NULL);
    }

    /**
     * @param Builder $query
     */
    private function _buildForActivity(Builder &$query)
    {
        if ($this->_bothActivityWindowsSet()) {
            $this->_buildForActivityWindows($query);
        }
    }

    /**
     * @param Builder $query
     */
    private function _buildForActivityWindows(Builder &$query)
    {
        return $query
            ->whereIn('id', TranscribeStat::query()
                ->whereDate('timeSaved', '>=', $this->activityWindowStart)
                ->whereDate('timeSaved', '<', $this->activityWindowEnd)
                ->pluck('mediaItemID')
                ->toArray()
            );
    }

    /**
     * @param \DateTime $activityWindowStart
     * @param \DateTime $activityWindowEnd
     */
    public function setWindowOfActivity(\DateTime $activityWindowStart, \DateTime $activityWindowEnd)
    {
        if ($activityWindowStart < $activityWindowEnd) {
            $this->activityWindowStart = $activityWindowStart;
            $this->activityWindowEnd = $activityWindowEnd;
        } else {
            $this->activityWindowStart = $activityWindowEnd;
            $this->activityWindowEnd = $activityWindowStart;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function mediaItemQuery()
    {
        $query = MediaItem::query();

        $this->_buildForActivity($query);

        return $query;
    }

    /**
     * @return int|mixed
     */
    public function totalDuration()
    {
        return static::mediaItemQuery()->sum('duration');
    }

}
