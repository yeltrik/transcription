<?php

namespace Yeltrik\Transcription\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Yeltrik\Transcription\app\report\ReportWindows;
use Yeltrik\Transcription\app\report\TranscribedVideos;


class ReportController extends Controller
{

    /**
     * ReportController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $this->authorize('report');
        $this->authorize('transcription');

        $reportWindows = ReportWindows::getWindows();
        $transcribedVideos = new TranscribedVideos();
        $transcribedVideos->forWindows(new ReportWindows());
        $transcribedVideosTableArray = $transcribedVideos->getAsTableArray();

        return view('transcription::report.transcription.index', compact(
            'reportWindows',
            'transcribedVideosTableArray'
        ));
    }

}
